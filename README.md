myservice-deployment.yml:
Fichier contenant les informations de configuration de la gateway, du proxy et du service et de son déploiement.


Ces fichiers permettent l'utilisation de l'application, ainsi que de microservices associés (Proxy, Gateway).

Elements de configuration du .yaml:
Gateway: Passerelle par défaut
Service: service exploité
Déploiement: déploiement du service
VirtualService: Proxy 
